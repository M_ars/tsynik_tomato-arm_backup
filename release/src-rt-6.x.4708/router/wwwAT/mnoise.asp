<html lang="en">
	<!--
	Tomato GUI
	Copyright (C) 2006-2010 Jonathan Zarate
	http://www.polarcloud.com/tomato/

	For use with Tomato Firmware only.
	No part of this file may be used without permission.
	-->
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<meta name="robots" content="noindex,nofollow">
		<title>[<% ident(); %>] <% translate("Measuring Noise"); %>...</title>

		<style>
			body {
				font-family: Verdana;
				font-size: 14px;
				background: #bfbfbf;
				color: #585858;
				line-height: 2em;
			}

			#loader {
				width:100%;
				max-width: 480px;
				text-align: center;
				margin: 15% auto;
				padding: 20px;
			}

			/* Pure CSS preloader */
			.spinner {
				display: inline-block;
				width: 36px;
				height: 36px;
				box-sizing: border-box;
				vertical-align: middle;
				border: solid 2px transparent;
				border-top-color: #3C3C3C;
				border-bottom-color: #3C3C3C;
				border-radius: 50%;
				-webkit-border-radius: 50%;
				-webkit-animation: tomato-spinner 600ms linear infinite;
				animation: tomato-spinner 600ms linear infinite;
			}
			@-webkit-keyframes tomato-spinner { 0%   { -webkit-transform: rotate(0deg); }  100% { -webkit-transform: rotate(360deg); } }
			@keyframes tomato-spinner { 0%   { transform: rotate(0deg); }  100% { transform: rotate(360deg); } }
		</style>
		<script type="text/javascript">
			function tick()
			{
				t.innerHTML = tock;
				if (--tock >= 0) setTimeout(tick, 1000);
				else history.go(-1);
			}
			function init()
			{
				t = document.getElementById('time');
				tock = 15;
				tick();
			}
		</script>
	</head>

	<body onload="init()" onclick="go()">

		<div id="loader">
			<div class="spinner"></div><br/><br/>
			<b><% translate("Measuring radio noise floor"); %>...</b><br/>
			<% translate("Wireless access has been temporarily disabled for"); %> <span id="time">15</span> <% translate("s."); %>
		</div>

	</body>
</html>
